# WebM to GIF Converter

Tired of weird GIF colors when you convert your .webm files using online converter? This one's for you!

## How to Use

1. Clone this repo.
2. Go to the directory: `cd webm-to-gif-converter`.
3. Change permission settings for run.sh: `chmod +x run.sh`.
4. Copy your .webm file(s).
5. Run it! `./run.sh`
