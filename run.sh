#!/bin/bash
for video in *.webm
do
 ffmpeg -y -i $video -vf palettegen palette.png
 ffmpeg -y -i $video -i palette.png -filter_complex paletteuse -r 10 ${video%.*}.gif
done
rm *.webm
rm palette.png
